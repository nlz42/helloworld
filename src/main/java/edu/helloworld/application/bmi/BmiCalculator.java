package edu.helloworld.application.bmi;

import edu.helloworld.web.model.BmiDTO;

public class BmiCalculator {

    public void calcBmi(BmiDTO bmiDTO) {

        double bmi =  bmiDTO.getWeight() / (Math.pow((bmiDTO.getHigh()/100.0), 2));
        bmiDTO.setBmi(bmi);
    }
}
