package edu.helloworld.web.controller;

import edu.helloworld.application.bmi.BmiCalculator;
import edu.helloworld.web.model.BmiDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class WebController {

    @GetMapping(value = "/")
    public String index() {
        return "index";
    }

    @GetMapping(value = "/nils")
    public String nils() {
        return "nils";
    }

    @GetMapping(value = "/bmi")
    public String bmiCalculator(Model model) {
        model.addAttribute("bmiDTO", new BmiDTO());
        return "bmi";
    }

    @PostMapping(value = "/bmi")
    public String myBMI(@ModelAttribute BmiDTO bmiDTO, Model model) {
        BmiCalculator calculator = new BmiCalculator();
        calculator.calcBmi(bmiDTO);
        model.addAttribute("bmiDTO", bmiDTO);
        return "bmi";
    }
}
